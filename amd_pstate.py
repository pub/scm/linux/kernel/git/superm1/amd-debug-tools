#!/usr/bin/python3
# SPDX-License-Identifier: MIT
"""CPPC triage script for AMD systems"""

import sys
import os
import re
import subprocess
import logging
import argparse
import struct
from datetime import date


class Defaults:  # pylint: disable=too-few-public-methods
    """Default values for the script"""

    log_prefix = "amd_pstate_report"
    log_suffix = "txt"


class Colors:  # pylint: disable=too-few-public-methods
    """ANSI color codes for terminal output"""

    DEBUG = "\033[90m"
    HEADER = "\033[95m"
    OK = "\033[94m"
    WARNING = "\033[32m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    UNDERLINE = "\033[4m"


class MSR:  # pylint: disable=too-few-public-methods
    """MSR addresses for CPPC"""

    MSR_AMD_CPPC_CAP1 = 0xC00102B0
    MSR_AMD_CPPC_ENABLE = 0xC00102B1
    MSR_AMD_CPPC_CAP2 = 0xC00102B2
    MSR_AMD_CPPC_REQ = 0xC00102B3
    MSR_AMD_CPPC_STATUS = 0xC00102B4


def AMD_CPPC_CAP_LOWEST_PERF(x):
    """Return the lowest performance value from the given input."""
    return x & 0xFF


def AMD_CPPC_CAP_LOWNONLIN_PERF(x):
    """Return the lowest nonlinear performance value from the given input."""
    return (x >> 8) & 0xFF


def AMD_CPPC_CAP_NOMINAL_PERF(x):
    """Return the nominal performance value from the given input."""
    return (x >> 16) & 0xFF


def AMD_CPPC_CAP_HIGHEST_PERF(x):
    """Return the highest performance value from the given input."""
    return (x >> 24) & 0xFF


def AMD_CPPC_MAX_PERF(x):
    """Return the maximum performance value from the given input."""
    return x & 0xFF


def AMD_CPPC_MIN_PERF(x):
    """Return the minimum performance value from the given input."""
    return (x >> 8) & 0xFF


def AMD_CPPC_DES_PERF(x):
    """Return the desired performance value from the given input."""
    return (x >> 16) & 0xFF


def AMD_CPPC_EPP_PERF(x):
    """Return the energy performance preference value from the given input."""
    return (x >> 24) & 0xFF


class Headers:  # pylint: disable=too-few-public-methods
    """Header strings for the script"""

    LogDescription = "Location of log file"
    InstallAction = "Attempting to install"
    RerunAction = "Running this script as root will attempt to install it"
    MissingPyudev = "Udev access library `pyudev` is missing"
    MissingPandas = "Data library `pandas` is missing"
    MissingTabulate = "Data library `tabulate` is missing"


class DistroPackage:
    """Class for handling distro-specific packages"""

    def __init__(self, deb, rpm, arch, pip, root):
        self.deb = deb
        self.rpm = rpm
        self.arch = arch
        self.pip = pip
        self.root = root

    def install(self, distro):
        """Install the package for the given distro."""
        if not self.root:
            sys.exit(1)
        if distro in ("ubuntu", "debian"):
            if not self.deb:
                return False
            installer = ["apt", "install", self.deb]
        elif distro == "fedora":
            if not self.rpm:
                return False
            release = read_file("/usr/lib/os-release")
            variant = None
            for line in release.split("\n"):
                if line.startswith("VARIANT_ID"):
                    variant = line.split("=")[-1]
            if variant != "workstation":
                return False
            installer = ["dnf", "install", "-y", self.rpm]
        elif distro == "arch" or os.path.exists("/etc/arch-release"):
            installer = ["pacman", "-Sy", self.arch]
        else:
            try:
                import pip
            except ModuleNotFoundError:
                self.pip = False
            if not self.pip:
                return False
            installer = ["python3", "-m", "pip", "install", "--upgrade", self.pip]
        subprocess.check_call(installer)
        return True


class PyUdevPackage(DistroPackage):
    """Class for handling the pyudev package"""

    def __init__(self, root):
        super().__init__(
            deb="python3-pyudev",
            rpm="python3-pyudev",
            arch="python-pyudev",
            pip="pyudev",
            root=root,
        )


class PandasPackage(DistroPackage):
    """Class for handling the pandas package"""

    def __init__(self, root):
        super().__init__(
            deb="python3-pandas",
            rpm="python3-pandas",
            arch="python-pandas",
            pip="pandas",
            root=root,
        )


class TabulatePackage(DistroPackage):
    """Class for handling the tabulate package"""

    def __init__(self, root):
        super().__init__(
            deb="python3-tabulate",
            rpm="python3-tabulate",
            arch="python-tabulate",
            pip="tabulate",
            root=root,
        )


def read_file(fn):
    """Read the contents of a file and return it as a string."""
    with open(fn, "r", encoding="utf-8") as r:
        return r.read().strip()


def print_color(message, group):
    """Print a message with a color based on the group."""
    prefix = "%s " % group
    suffix = Colors.ENDC
    if group == "🚦":
        color = Colors.WARNING
    elif group == "🦟":
        color = Colors.DEBUG
    elif any(mk in group for mk in ["❌", "👀"]):
        color = Colors.FAIL
    elif any(mk in group for mk in ["✅", "🔋", "🐧", "💻", "🫆", "○"]):
        color = Colors.OK
    else:
        color = group
        prefix = ""

    log_txt = f"{prefix}{message}".strip()
    if any(c in color for c in [Colors.OK, Colors.HEADER, Colors.UNDERLINE]):
        logging.info(log_txt)
    elif color == Colors.WARNING:
        logging.warning(log_txt)
    elif color == Colors.FAIL:
        logging.error(log_txt)
    else:
        logging.debug(log_txt)

    if "TERM" in os.environ and os.environ["TERM"] == "dumb":
        suffix = ""
        color = ""
    print(f"{prefix}{color}{message}{suffix}")


def fatal_error(message):
    """Print an error message and exit the program."""
    print_color(message, "👀")
    sys.exit(1)


class AmdPstateTriage:
    """Class for handling the triage process"""

    def show_install_message(self, message):
        """Show a message indicating the installation action."""
        action = Headers.InstallAction if self.root_user else Headers.RerunAction
        message = f"{message}. {action}."
        print_color(message, "👀")

    def __init__(self, arg):
        # for saving a log file for analysis
        logging.basicConfig(
            format="%(asctime)s %(levelname)s:\t%(message)s",
            filename=arg,
            filemode="w",
            level=logging.DEBUG,
        )

        self.root_user = os.geteuid() == 0

        try:
            import distro

            self.distro = distro.id()
            self.pretty_distro = distro.distro.os_release_info()["pretty_name"]
        except ModuleNotFoundError:
            fatal_error("Missing python-distro package, unable to identify distro")

        try:
            import pyudev

            self.context = pyudev.Context()
        except ModuleNotFoundError:
            self.context = False

        if not self.context:
            self.show_install_message(Headers.MissingPyudev)
            package = PyUdevPackage(self.root_user)
            package.install(self.distro)
            try:
                from pyudev import Context
            except ModuleNotFoundError:
                fatal_error("Missing python-pyudev package, unable to identify devices")
            self.context = Context()

        try:
            from pandas import DataFrame

            self.pandas = True
        except ModuleNotFoundError:
            self.pandas = False
        except ImportError:
            self.pandas = False

        if not self.pandas:
            self.show_install_message(Headers.MissingPandas)
            package = PandasPackage(self.root_user)
            package.install(self.distro)
            try:
                from pandas import DataFrame

                self.pandas = True
            except ModuleNotFoundError:
                fatal_error("Missing pandas package, unable to gather data")

        try:
            from tabulate import tabulate

            self.tabulate = True
        except ModuleNotFoundError:
            self.tabulate = False

        if not self.tabulate:
            self.show_install_message(Headers.MissingTabulate)
            package = TabulatePackage(self.root_user)
            package.install(self.distro)
            try:
                from tabulate import tabulate

                self.tabulate = True
            except ModuleNotFoundError:
                fatal_error("Missing python-tabulate package, unable to display data")

    def gather_amd_pstate_info(self):
        for f in ("status", "prefcore"):
            p = os.path.join("/", "sys", "devices", "system", "cpu", "amd_pstate", f)
            if os.path.exists(p):
                print_color(f"'{f}':\t{read_file(p)}", "🫆")

    def gather_kernel_info(self):
        """Gather kernel information"""
        print_color(f"Kernel:\t{os.uname().release}", "🐧")

    def gather_scheduler_info(self):
        """Gather information about the scheduler"""
        procfs = os.path.join("/", "proc", "sys", "kernel", "sched_itmt_enabled")
        debugfs = os.path.join(
            "/", "sys", "kernel", "debug", "x86", "sched_itmt_enabled"
        )
        for p in [procfs, debugfs]:
            if os.path.exists(p):
                val = read_file(p)
                print_color(f"ITMT:\t{val}", "🐧")

    def gather_cpu_info(self):
        """Gather a dataframe of CPU information"""
        import pandas as pd
        from tabulate import tabulate

        df = pd.DataFrame(
            columns=[
                "CPU #",
                "CPU Min Freq",
                "CPU Nonlinear Freq",
                "CPU Max Freq",
                "Scaling Min Freq",
                "Scaling Max Freq",
                "Energy Performance Preference",
                "Prefcore",
                "Boost",
            ]
        )

        for device in self.context.list_devices(subsystem="cpu"):
            p = os.path.join(device.sys_path, "cpufreq")
            if not os.path.exists(p):
                continue
            row = [
                int(re.findall(r"\d+", f"{device.sys_name}")[0]),
                read_file(os.path.join(p, "cpuinfo_min_freq")),
                read_file(os.path.join(p, "amd_pstate_lowest_nonlinear_freq")),
                read_file(os.path.join(p, "cpuinfo_max_freq")),
                read_file(os.path.join(p, "scaling_min_freq")),
                read_file(os.path.join(p, "scaling_max_freq")),
                read_file(os.path.join(p, "energy_performance_preference")),
                read_file(os.path.join(p, "amd_pstate_prefcore_ranking")),
                read_file(os.path.join(p, "boost")),
            ]
            df = pd.concat(
                [pd.DataFrame([row], columns=df.columns), df], ignore_index=True
            )

        cpuinfo = read_file("/proc/cpuinfo")
        model = re.findall(r"model name\s+:\s+(.*)", cpuinfo)[0]
        print_color(f"CPU:\t\t{model}", "💻")

        df = df.sort_values(by="CPU #")
        print_color(
            "Per-CPU sysfs files\n%s"
            % tabulate(df, headers="keys", tablefmt="psql", showindex=False),
            "🔋",
        )

    def gather_msrs(self):
        """Gather MSR information"""

        def read_msr(msr, cpu):
            p = f"/dev/cpu/{cpu}/msr"
            if not os.path.exists(p) and self.root_user:
                os.system("modprobe msr")
            try:
                f = os.open(p, os.O_RDONLY)
            except OSError as exc:
                raise PermissionError from exc
            try:
                os.lseek(f, msr, os.SEEK_SET)
                val = struct.unpack("Q", os.read(f, 8))[0]
            except OSError as exc:
                raise PermissionError from exc
            finally:
                os.close(f)
            return val

        import pandas as pd
        from tabulate import tabulate

        cpus = []
        for device in self.context.list_devices(subsystem="cpu"):
            cpu = int(re.findall(r"\d+", f"{device.sys_name}")[0])
            cpus.append(cpu)
        cpus.sort()

        df = pd.DataFrame(
            columns=[
                "CPU #",
                "Min Perf",
                "Max Perf",
                "Desired Perf",
                "Energy Performance Perf",
            ]
        )

        msr_df = pd.DataFrame(
            columns=[
                "CPU #",
                "Enable",
                "Status",
                "Cap 1",
                "Cap 2",
                "Request",
            ]
        )

        cap_df = pd.DataFrame(
            columns=[
                "CPU #",
                "Lowest Perf",
                "Nonlinear Perf",
                "Nominal Perf",
                "Highest Perf",
            ]
        )

        try:
            for cpu in cpus:
                enable = read_msr(MSR.MSR_AMD_CPPC_ENABLE, cpu)
                status = read_msr(MSR.MSR_AMD_CPPC_STATUS, cpu)
                cap1 = read_msr(MSR.MSR_AMD_CPPC_CAP1, cpu)
                cap2 = read_msr(MSR.MSR_AMD_CPPC_CAP2, cpu)

                req = read_msr(MSR.MSR_AMD_CPPC_REQ, cpu)
                row = [
                    cpu,
                    AMD_CPPC_MIN_PERF(req),
                    AMD_CPPC_MAX_PERF(req),
                    AMD_CPPC_DES_PERF(req),
                    AMD_CPPC_EPP_PERF(req),
                ]
                df = pd.concat(
                    [pd.DataFrame([row], columns=df.columns), df], ignore_index=True
                )

                row = [
                    cpu,
                    enable,
                    status,
                    hex(cap1),
                    hex(cap2),
                    hex(req),
                ]
                msr_df = pd.concat(
                    [pd.DataFrame([row], columns=msr_df.columns), msr_df],
                    ignore_index=True,
                )

                row = [
                    cpu,
                    AMD_CPPC_CAP_LOWEST_PERF(cap1),
                    AMD_CPPC_CAP_LOWNONLIN_PERF(cap1),
                    AMD_CPPC_CAP_NOMINAL_PERF(cap1),
                    AMD_CPPC_CAP_HIGHEST_PERF(cap1),
                ]
                cap_df = pd.concat(
                    [pd.DataFrame([row], columns=cap_df.columns), cap_df],
                    ignore_index=True,
                )

        except FileNotFoundError:
            print_color("Unabled to check MSRs: MSR kernel module not loaded", "❌")
            return False
        except PermissionError:
            if not self.root_user:
                print_color("Run as root to check MSRs", "🚦")
            else:
                print_color("MSR checks unavailable", "🚦")
            return

        msr_df = msr_df.sort_values(by="CPU #")
        print_color(
            "CPPC MSRs\n%s"
            % tabulate(msr_df, headers="keys", tablefmt="psql", showindex=False),
            "🔋",
        )

        cap_df = cap_df.sort_values(by="CPU #")
        print_color(
            "MSR_AMD_CPPC_CAP1 (decoded)\n%s"
            % tabulate(cap_df, headers="keys", tablefmt="psql", showindex=False),
            "🔋",
        )

        df = df.sort_values(by="CPU #")
        print_color(
            "MSR_AMD_CPPC_REQ (decoded)\n%s"
            % tabulate(df, headers="keys", tablefmt="psql", showindex=False),
            "🔋",
        )

    def run(self):
        """Run the triage process"""
        self.gather_kernel_info()
        self.gather_amd_pstate_info()
        self.gather_scheduler_info()
        try:
            self.gather_cpu_info()
        except FileNotFoundError:
            print_color("Unable to gather CPU information", "❌")
        self.gather_msrs()


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Collect useful information for debugging amd-pstate issues.",
        epilog="Arguments are optional, and if they are not provided will prompted.\n"
        "To use non-interactively, please populate all optional arguments.",
    )
    parser.add_argument(
        "--log",
        help=Headers.LogDescription,
    )
    return parser.parse_args()


def configure_log(arg):
    """Configure the log file name based on the provided argument or user input."""
    if not arg:
        fname = f"{Defaults.log_prefix}-{date.today()}.{Defaults.log_suffix}"
        arg = input(f"{Headers.LogDescription} (default {fname})? ")
        if not arg:
            arg = fname
    return arg


if __name__ == "__main__":
    args = parse_args()
    log = configure_log(args.log)
    triage = AmdPstateTriage(log)
    triage.run()
